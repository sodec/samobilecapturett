
#
#  Be sure to run `pod spec lint SAMobileCapture.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "SAMobileCapture"
  spec.version      = "1.1.49"
  spec.summary      = "Mobile capture application for text recognition."

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = "Mobile capture application for text recognition."

  spec.homepage     = "http://sodecapss.com"
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See https://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #


    spec.license      = "commercial"



  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  spec.author             = { "Erkan Şirin" => "erkan.sirin@sodecapps.com" }
  # Or just: spec.author    = "Erkan Şirin"
  # spec.authors            = { "Erkan Şirin" => "erkan.sirin@sodecapps.com" }
  # spec.social_media_url   = ""

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  # spec.platform     = :ios
   spec.platform     = :ios, "10.0"

  #  When using multiple platforms
  # spec.ios.deployment_target = "5.0"
  # spec.osx.deployment_target = "10.7"
  # spec.watchos.deployment_target = "2.0"
  # spec.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  spec.source       = { :git => "https://bitbucket.org/sodec/samobilecapturett.git", :tag => "#{spec.version}" }
  #spec.source       = { :path =>'/pod/samobilecapturett' }


  spec.swift_version = '5.0'

  spec.source_files = 'SAMobileCapture.xcframework/ios-arm64/SAMobileCapture.framework/Headers/*.h'
  spec.public_header_files = 'SAMobileCapture.xcframework/ios-arm64/SAMobileCapture.framework/Headers/*.h'
  spec.vendored_frameworks = 'SAMobileCapture.xcframework'
  



  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # spec.resource  = "icon.png"
  # spec.resources = "Resources/*.png"

  # spec.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

spec.ios.frameworks = ['AVFoundation', 'AudioToolbox', 'Accelerate', 'CoreGraphics', 'CoreImage', 'CoreMedia', 'CoreVideo', 'CoreText', 'CoreNFC', 'CryptoKit', 'CryptoTokenKit', 'Foundation', 'MessageUI', 'MobileCoreServices', 'OpenGLES', 'QuartzCore', 'Photos', 'ReplayKit', 'UIKit', 'Security', 'SystemConfiguration']  
  

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
    # spec.dependency "OpenCV", "3.4.2"
    # spec.dependency "GoogleMLKit/FaceDetection"
     #spec.dependency "GoogleMLKit/TextRecognition"
     #spec.dependency "GoogleMLKit/BarcodeScanning"
        spec.dependency 'OpenSSL-Universal','1.1.180'

spec.xcconfig = { 'OTHER_LDFLAGS' => '-weak_framework CoreNFC -weak_framework CryptoKit -weak_framework CryptoTokenKit' }
  spec.pod_target_xcconfig = { 'OTHER_LDFLAGS' => '-ObjC -l\"c++\" -l\"sqlite3\" -l\"z\" -framework AFNetworking -framework AVFoundation -framework AudioToolbox -framework Accelerate -framework AssetsLibrary -framework CoreGraphics -framework CoreImage -framework CoreMedia -framework CoreVideo -framework CoreText -framework CoreTelephony -weak_framework CoreNFC -weak_framework CryptoKit -weak_framework CryptoTokenKit -framework FBLPromises -framework Foundation -framework GTMSessionFetcher -framework GoogleDataTransport -framework GoogleToolboxForMac -framework GoogleUtilities -framework GoogleUtilitiesComponents -framework LocalAuthentication -framework Lottie -framework MLImage -framework MLKitBarcodeScanning -framework MLKitCommon -framework MLKitFaceDetection -framework MLKitTextRecognition -framework MLKitVision -framework OpenSSL -framework TFLTensorFlowLite -framework TensorFlowLiteC -framework nanopb -framework opencv2 -framework MessageUI -framework MobileCoreServices -framework OpenGLES -framework QuartzCore -framework Photos -framework ReplayKit -framework UIKit -framework Security -framework SystemConfiguration' }


end


