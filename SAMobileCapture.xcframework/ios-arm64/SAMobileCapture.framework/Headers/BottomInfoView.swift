//
//  BottomInfoView.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 21.11.2022.
//  Copyright © 2022 Sodec Bilisim Teknolojileri. All rights reserved.
//


import UIKit
import AVFoundation

@objc public class BottomInfoView: UIView {
    
    @IBOutlet weak var labelContainer: UIView!
    @objc @IBOutlet weak var descriptionLabel: UILabel!
    var view: UIView!
    
    @objc public func setDesctiption(description:String){
        
        descriptionLabel.text = description
        self.layoutIfNeeded()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: { [self] in
        print("descriptionLabel.frame.height : ",descriptionLabel.frame.height)
        })
    }
    
    @objc public func getLabelContainerHeight() -> CGFloat{
        
            return descriptionLabel.frame.height
        
    }
    
    @objc public func setBgColor(color : UIColor){
        
        labelContainer.backgroundColor = color
        
    }
    
    @objc public func setLabelFontAndSize(fontName : String,fontSize : CGFloat){
        
        descriptionLabel.font = UIFont(name: fontName , size: fontSize)
        
    }
    

    
    @objc public override init(frame: CGRect) {
        super.init(frame: frame)
        print("frame : ",frame)
        self.bounds = frame
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
}
private extension BottomInfoView {
    
    func xibSetup() {
        
        
        
        backgroundColor = UIColor.clear
        
        
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        if let viewCreate = nib.instantiate(withOwner: self, options: nil).first as? UIView {
            view = viewCreate
            print("bounds.size.width : ",bounds.size.width)
            print("bounds.size.height : ",bounds.size.height)
            view.frame = bounds
            addSubview(view)
            
            view.translatesAutoresizingMaskIntoConstraints = false
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                          options: [],
                                                          metrics: nil,
                                                          views: ["childView": view as UIView]))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                          options: [],
                                                          metrics: nil,
                                                          views: ["childView": view as UIView]))
        }
        
        
        
       
        
        
        
    }
    
   
    
   
}


