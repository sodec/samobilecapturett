//
//  SADataLoader.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 1.06.2022.
//  Copyright © 2022 Sodec Inc. All rights reserved.
//

import Foundation
@objc public class SAGetData: NSObject {

    @objc public func loadDataFromDocumentPath(fileName : String, isEncrypted : Bool,completion: @escaping (Data) -> ())
{
    var returnData : Data?
  
    let url = getDocumentsDirectory().appendingPathComponent(fileName)
        //if fileManager.fileExists(atPath: url.path) {
            
            if isEncrypted {
                
                do {
                    let data = try Data(contentsOf: url)
                    returnData = SACrypto.decryptData(data)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                        if let dataLoaded = returnData {
                            completion(dataLoaded)
                        }
                    })
                    
                }catch{
                    print("error ecrpt: ",error)
                }
                
            }else{
                do {
                    returnData = try Data(contentsOf: url)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                        if let dataLoaded = returnData {
                            completion(dataLoaded)
                        }
                    })
                }catch{
                    print("error normal: ",error)
                }
            }
            
            
//        } else {
//            print("FILE NOT AVAILABLE")
//        }
        

}
    
    @objc public func loadDataFromFilePath(filePath : String, isEncrypted : Bool,completion: @escaping (Data?,Error?) -> ())
{
    var returnData : Data?
    let url = URL(fileURLWithPath: filePath)
  

        //if fileManager.fileExists(atPath: url.path) {
            
            if isEncrypted {
                
                do {
                    let data = try Data(contentsOf: url)
                    returnData = SACrypto.decryptData(data)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            completion(returnData,nil)
                    })
                    
                }catch{
                    completion(nil,error)
                }
                
            }else if filePath.contains("mp4"){
                
                do {
                    let data = try Data(contentsOf: url)
                    //returnData = SACrypto.decryptData(data)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            completion(data,nil)
                    })
                    
                }catch{
                    completion(nil,error)
                }
                
                
            } else{
                do {
                    returnData = try Data(contentsOf: url)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        if let dataLoaded = returnData {
                            completion(dataLoaded,nil)
                        }
                    })
                }catch{
                    completion(nil,error)
                }
            }
            
            
//        } else {
//            print("FILE NOT AVAILABLE")
//        }
        

}
    
    @objc func getDocumentsDirectory() -> URL {
        // find all possible documents directories for this user
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)

        // just send back the first one, which ought to be the only one
        return paths[0]
    }

}
