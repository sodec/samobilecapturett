/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

@interface SAGIFParams : NSObject

@property (nonatomic, assign) BOOL enabled;
@property (strong, nonatomic, readwrite) NSString *introFileName;
@property (strong, nonatomic, readwrite) NSString *turnHeadRightFileName;
@property (strong, nonatomic, readwrite) NSString *turnHeadLeftFileName;
@property (strong, nonatomic, readwrite) NSString *blinkEyesFileName;

@end
