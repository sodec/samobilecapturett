//
//  SANFC.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 8.03.2021.
//  Copyright © 2021 Sodec Bilisim Teknolojileri. All rights reserved.
//

import Foundation
import UIKit
import CoreNFC
//delegates function names

@objc public protocol SAChipDelegate{
    
    func didReadChipDone(saChipData: SAChipData)
    func didReadChipDoneWithError(error: NSError)
    func didReadChipCancel()
    //func didReadChipLog(logString : String)
    //func didReadChipLogError(logError : NSError)
}

@available(iOS 13, *)
@objcMembers public class SAReadChip : NSObject,SAReadDelegate,SAChipDelegate {
//    public func didReadChipLogError(logError: NSError) {
//        
//    }
    
//    public func didReadChipLog(logString: String) {
//        self.delegate?.didReadChipDoneWithError(error: NSError(domain: "error logs", code: 1, userInfo: ["log" : logString]))
//    }
    
   
    public func didReadChipDone(saChipData: SAChipData) {
        
    }

    public func didReadChipDoneWithError(error: NSError) {
  
        self.delegate?.didReadChipDoneWithError(error: error)
    }
    
    public func didReadDone(saChipData: SAChipData) {
     
        self.delegate?.didReadChipDone(saChipData: saChipData)
    }
    
    public func didReadChipCancel()
    {
        self.delegate?.didReadChipCancel()
     
    }
    
    var delegate :SAChipDelegate?
    let chipReader = NFCWrapper()
    
    public var nfcReadingTittle = "Lütfen kartınızı haraket ettirmeyin"
    @objc public func setParam(nfcReadingTittle:NSString){
        self.nfcReadingTittle = "\(nfcReadingTittle)"
    }
    
    public var nfcInfoTittle = "NFC Chip özelliği olan dökümanı cihazın üst kısmına tutunuz okuma bitene kadar çekmeyiniz."
    @objc public func setParam(nfcInfoTittle:NSString){
        self.nfcInfoTittle = "\(nfcInfoTittle)"
    }
    
    public var validateDocumentTittle = "Döküman doğrulanıyor"
    @objc public func setParam(validateDocumentTittle:NSString){
        self.validateDocumentTittle = "\(validateDocumentTittle)"
    }
    
    public var invalidTagTittle = "Geçersiz tag."
    @objc public func setParam(invalidTagTittle:NSString){
        self.invalidTagTittle = "\(invalidTagTittle)"
    }
  
    public var multiTagTittle = "Geçersiz tag."
    @objc public func setParam(multiTagTittle:NSString){
        self.multiTagTittle = "\(multiTagTittle)"
    }
    
    public var connectionErrorTittle = "Bağlantı hatası. Lütfen tekrar deneyin."
    @objc public func setParam(connectionErrorTittle:NSString){
        self.connectionErrorTittle = "\(connectionErrorTittle)"
    }
    
    public var mrzErrorTittle = "MRZ anahtar uyumsuzdur."
    @objc public func setParam(mrzErrorTittle:NSString){
        self.mrzErrorTittle = "\(mrzErrorTittle)"
    }
    
    public var readingErrorTittle = "Döküman okuma hatası."
    @objc public func setParam(readingErrorTittle:NSString){
        self.readingErrorTittle = "\(readingErrorTittle)"
    }
    
    public var readingSuccessTittle = "Döküman okuma başarılı."
    @objc public func setParam(readingSuccessTittle:NSString){
        self.readingSuccessTittle = "\(readingSuccessTittle)"
    }
    
    public var corruptedDataTittle = "Part of returned data may be corrupted"
    @objc public func setParam(corruptedDataTittle:NSString){
        self.corruptedDataTittle = "\(corruptedDataTittle)"
    }
    
    public var fileInvalidatedTittle = "file invalidated"
    @objc public func setParam(fileInvalidatedTittle:NSString){
        self.fileInvalidatedTittle = "\(fileInvalidatedTittle)"
    }
    
    public var fCIErrorTittle = "FCI not formatted according to ISO7816-4 section 5.1.5"
    @objc public func setParam(fCIErrorTittle:NSString){
        self.fCIErrorTittle = "\(fCIErrorTittle)"
    }
    
    public var transmissionErrorTittle = "Secured Transmission not supported"
    @objc public func setParam(transmissionErrorTittle:NSString){
        self.transmissionErrorTittle = "\(transmissionErrorTittle)"
    }
    
    public var memoryErrorTittle = "Memory failure"
    @objc public func setParam(memoryErrorTittle:NSString){
        self.memoryErrorTittle = "\(memoryErrorTittle)"
    }
    
    public var commandErrorTittle = "Invalid command"
    @objc public func setParam(commandErrorTittle:NSString){
        self.commandErrorTittle = "\(commandErrorTittle)"
    }
    
    public var authenticationErrorTittle = "Authentication error"
    @objc public func setParam(authenticationErrorTittle:NSString){
        self.authenticationErrorTittle = "\(authenticationErrorTittle)"
    }
    
    public var invalidDataErrorTittle = "Invalid data"
    @objc public func setParam(invalidDataErrorTittle:NSString){
        self.invalidDataErrorTittle = "\(invalidDataErrorTittle)"
    }



    @objc public func readChip(documentNumber: String, dateOfBirth: SADate, dateOfExpiry: SADate,delegate:SAChipDelegate,masterListURL:URL){
        
        self.delegate = delegate
        chipReader.delegate = self
    
        chipReader.nfcReadingTittle = self.nfcReadingTittle
        chipReader.nfcInfoTittle = self.nfcInfoTittle
        chipReader.validateDocumentTittle = self.validateDocumentTittle
        chipReader.invalidTagTittle = self.invalidTagTittle
        chipReader.multiTagTittle = self.multiTagTittle
        chipReader.connectionErrorTittle = self.connectionErrorTittle
        chipReader.mrzErrorTittle = self.mrzErrorTittle
        chipReader.readingErrorTittle = self.readingErrorTittle
        chipReader.readingSuccessTittle = self.readingSuccessTittle
        chipReader.corruptedDataTittle = self.corruptedDataTittle
        chipReader.fileInvalidatedTittle = self.fileInvalidatedTittle
        chipReader.fCIErrorTittle = self.fCIErrorTittle
        chipReader.transmissionErrorTittle = self.transmissionErrorTittle
        chipReader.memoryErrorTittle = self.memoryErrorTittle
        chipReader.commandErrorTittle = self.commandErrorTittle
        chipReader.authenticationErrorTittle = self.authenticationErrorTittle
        chipReader.invalidDataErrorTittle = self.invalidDataErrorTittle
        
        chipReader.readNFC(documentNumber: documentNumber, dateOfBirth: dateOfBirth, dateOfExpiry: dateOfExpiry, certificateFileURL: masterListURL, delegate: self)
    }
}
