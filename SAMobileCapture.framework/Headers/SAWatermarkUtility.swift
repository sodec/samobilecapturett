//
//  SAFiligran.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 11.10.2023.
//  Copyright © 2023 Sodec Bilisim Teknolojileri. All rights reserved.
//

import Foundation

@objcMembers public class SAWatermarkUtility: NSObject {
    
    public var watermarkText : String = "Bu belge resmi işlemlerde kullanılamaz"
    public var fontAssetPath : String = ""
    public var fontName : String = "Helvetica Bold"
    public var maxTextSize : CGFloat = 55
    public var marginRatio : CGFloat = 50
    public var watermarkColor : UIColor = .red
    public var watermarkAlpha : CGFloat = 0.5
    
    @objc public override init() {
        super.init()
        
        //prepareVision()
    }
    
    
    public func createWatermark(image: UIImage) -> UIImage {
        
  
        
        let imageSize = image.size
        let scale: CGFloat = 1
        
        // Setup the font specific variables
        let textColor = watermarkColor
        
        let drawText = watermarkText
        

      
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, scale)
        let context = UIGraphicsGetCurrentContext()!
        image.draw(at: CGPoint.zero)
        

        let rightParagraph = NSMutableParagraphStyle()
        rightParagraph.alignment = .center
        
        let systemDynamicFontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .subheadline)
            let size = systemDynamicFontDescriptor.pointSize

        let textFont = UIFont(name: fontName, size: maxTextSize)!
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.paragraphStyle: rightParagraph,
        ]
        
        
        // Save current context.
        context.saveGState()
        // Move orgin.
       
        context.rotate(by: 0.5)
       
        
        let rect = CGRect(x: 50, y: 0, width: imageSize.width, height: imageSize.height)
        drawText.draw(in: rect, withAttributes: textFontAttributes)
        print("rect : ",rect)
        print("imageSize.width : ",imageSize.width)
        print("imageSize.height : ",imageSize.height)

   
        

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        context.restoreGState()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func rotateRect(_ rect: CGRect) -> CGRect {
        let x = rect.midX
        let y = rect.midY
        let transform = CGAffineTransform(translationX: x, y: y)
                                        .rotated(by: .pi / 2)
                                        .translatedBy(x: -x, y: -y)
        return rect.applying(transform)
    }
    
}
